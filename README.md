# Roommate

Roommate is a dynamic Django website created for York University's annual 2 day hackathon (Steacie Library Hackfest 2018).

The website was created for the purpose of helping students find roommates attending the same school.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

```
Python 3.6
```

```
pip
```

```
virtualenv (Not necessary but highly recommended)
```

```
Django 2.0.2
```

```
Pillow (PIL is the Python Imaging Library by Fredrik Lundh and Contributors.)
```
### Installing

Open powershell/terminal

###### Installing virtualenv and setting one up (after you've installed Python with pip in your system environment variables path)

```
>>> pip install virtualenv
>>> cd virtualenv/desired/location
>>> virtualenv name_of_your_virtual_env
```

###### Activate your virtualenv

```
>>> cd name_of_your_virtual_env
```
for windows: ```>>> scripts/activate```

mac/linux: ```>>> source bin/activate```


###### Install Django

```
>>> pip install django==2.0.2
```

###### Install Pillow

```
>>> pip install pillow
```

###### Clone the repo into your virtualenv
```
>>> git clone 
```

###### Now you're ready to run the server and access the website
```
>>> cd roommate
>>> python manage.py runserver
```

Now just ```localhost:8000``` in your browser

## Built With

* [Django](https://www.djangoproject.com/) - The web framework used
* [Bootstrap 4](https://getbootstrap.com/) - Frontend library

